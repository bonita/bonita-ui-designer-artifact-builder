# Bonita UI Designer Artifact Builder

[![Build](https://github.com/bonitasoft/bonita-ui-designer-artifact-builder/workflows/Build/badge.svg)](https://github.com/bonitasoft/bonita-ui-designer-artifact-builder/actions/workflows/build.yml)
 [![Sonarcloud Status](https://sonarcloud.io/api/project_badges/measure?project=bonitasoft_bonita-ui-designer-artifact-builder&metric=alert_status)](https://sonarcloud.io/dashboard?id=bonitasoft_bonita-ui-designer-artifact-builder)
[![GitHub release](https://img.shields.io/github/v/release/bonitasoft/bonita-ui-designer-artifact-builder?color=blue&label=Release)](https://github.com/bonitasoft/bonita-ui-designer-artifact-builder/releases)
[![Maven Central](https://img.shields.io/maven-central/v/org.bonitasoft.web/bonita-ui-designer-artifact-builder.svg?label=Maven%20Central&color=orange)](https://search.maven.org/search?q=g:%22org.bonitasoft%22%20AND%20a:%22ui-designer-artifact-builder%22)
[![License: GPL v2](https://img.shields.io/badge/License-GPL%20v2-yellow.svg)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)

Build pages designed with the UI Designer for your Bonita application or your own project.

## Quick start

### Pre-requisite

* [Maven][maven]
* [Java 11][java] for compilation

### Build

#### Using Maven

* Build it using maven `mvn clean package`

## To go further

### How does it work

The UI-designer is composed of a Java backend application and an AngularJs frontend.
It is packaged in a war file and provided by default in the [Bonita Studio][studio-repo]

It produces standalone AngularJs pages that are compatible with Bonita platform.

## Contribute


### Report issues

If you want to report an issue or a bug use our [official bugtracker](https://bonita.atlassian.net/projects/BBPMC)

### How to contribute
Before contributing, read the [guidelines][contributing.md]

### Build and Test

#### Build

You can build entire project using maven.
    
    mvn clean package   
    

## Resources

* [Documentation][documentation]


[maven]: https://maven.apache.org/
[java]: https://www.java.com/fr/download/
[uid-repo]: https://github.com/bonitasoft/bonita-ui-designer
[download]: https://www.bonitasoft.com/downloads
[documentation]: https://documentation.bonitasoft.com
[contributing.md]: https://github.com/bonitasoft/bonita-developer-resources/blob/master/CONTRIBUTING.MD

    
